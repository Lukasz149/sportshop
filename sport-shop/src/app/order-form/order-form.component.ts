import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Order } from '../order';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Product} from "../product/product";


@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent {
  @Input() products;
  @Output() orderEvent =  new EventEmitter();


  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onSubmit() {
    this.model.products = this.products;
    this.http.post("http://localhost:5000/orders", this.model).subscribe(data => {
      if(data['success']) {
        this.orderEvent.emit();
      }
    });

  }

  model: Order = new Order('', '', '', null, '');
}
