import {Injectable} from "@angular/core";
import {Product} from "./product/product";

@Injectable()
export class CartService {
  cart = [];

  getProductsFromCart() : Product[] {
    return this.cart;
  }

  addProductToCart(product : Product) : void {
    let added = false;
    for(let i = 0; i< this.cart.length; i++) {
      if(this.cart[i].product.name === product.name) {
        this.cart[i].amount++;
        added = true;
      }
    }
    if(!added){
      this.cart.push({
        product: product,
        amount: 1
      });
    }
  }

  deleteProductFromCartEvent(product : Product) : void {
    for(let i = 0; i< this.cart.length; i++) {
      if(this.cart[i].product.name === product.name) {
        if( this.cart[i].amount > 1) {
          this.cart[i].amount--;
          break;
        } else {
          this.cart.splice(i,1);
          break;
        }
      }
    }
  }

  getValueOfProductsInCart() : number {
    let val = 0;
    for(let i = 0; i < this.cart.length; i++) {
      val += this.cart[i].product.price* this.cart[i].amount;
    }
    return val;
  }

  getNumberOfProductInCart() : number {
    let val = 0;
    for(let i = 0; i < this.cart.length; i++) {
      val += this.cart[i].amount;
    }
    return val;
  }

  removeProductFromCart() {
    this.cart = [];
  }
}
