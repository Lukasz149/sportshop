import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'uniqueProducts'
})
export class UniqueProductsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return _.uniqBy(value, 'name');
  }

}
