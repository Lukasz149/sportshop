import {Product} from "./product/product";

export class Order {
  public name: string;
  public surname: string;
  public street: string;
  public houseNumber: number;
  public postCode: string;
  public products: Product[];
  public apartmentNumber?: number;

  constructor(
    name: string,
    surname: string,
    street: string,
    houseNumber: number,
    postCode: string,
    apartmentNumber?: number,
  ) {  }
}
