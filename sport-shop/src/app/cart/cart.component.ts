import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../product/product";
import {CartService} from "../cart.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  @Input() productsInCart;
  @Output() deleteProductFromCartEvent =  new EventEmitter<Product>();
  @Output() changeViewEvent =  new EventEmitter<string>();
  //cartElements: { [key:string]:{amount:number, product:Product, value:number}} = new Set();

  deleteProductFromCart(product: Product) {
    this.deleteProductFromCartEvent.emit(product);
  }

  changeView(view: string) {
    this.changeViewEvent.emit(view);
  }
  constructor() {
  }

  ngOnInit() {
  }

}
