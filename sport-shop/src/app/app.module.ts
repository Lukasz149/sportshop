import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';


import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { UniqueProductsPipe } from './unique-products.pipe';
import {HttpClientModule} from "@angular/common/http";
import { OrderFormComponent } from './order-form/order-form.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CartComponent,
    UniqueProductsPipe,
    OrderFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
