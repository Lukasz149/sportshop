import {Component, OnInit} from '@angular/core';
import {CartService} from "./cart.service";
import {Product} from "./product/product";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CartService]
})
export class AppComponent implements OnInit {
  products;
  category = '';
  numberOfProductsInCart : number = 0;
  valueOfProductsInCart : number = 0;
  currentView = '';

  constructor(private cartService : CartService, private http: HttpClient) {}

  ngOnInit(): void {
    this.initCart();
    this.currentView = 'app';
    this.http.get("http://localhost:5000/products").subscribe(data => {
      this.products = data;
    });

  }

  changeCategory(category) {
    let requestUrl = "http://localhost:5000/products/";
    if(category !== '') {
      requestUrl = "http://localhost:5000/products/category/" + category;
    }

    this.http.get(requestUrl).subscribe(data => {
      this.products = data;
      this.category = category;
    });
  }

  initCart() : void {
    this.numberOfProductsInCart = this.cartService.getNumberOfProductInCart();
    this.valueOfProductsInCart = this.getValueOfProductsInCart();
  }

  getValueOfProductsInCart() : number {
    return this.cartService.getValueOfProductsInCart();
  }

  addProductToCartEvent(product : Product) {
    this.cartService.addProductToCart(product);
    this.initCart()
  }

  orderEvent() {
    this.cartService.removeProductFromCart();
    this.initCart()
    this.changeView('app');
  }

  deleteProductFromCartEvent(product : Product) {
    this.cartService.deleteProductFromCartEvent(product);
    this.initCart()
  }

  changeViewEvent(view: string) {
    this.changeView(view);
  }

  changeView(view: string) : void {
    this.currentView = view;
  }

  getProductsFromCart() : Product[] {
    return this.cartService.getProductsFromCart();
  }
}
