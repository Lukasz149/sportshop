export class Product {
  public _id: string;
  public name: string;
  public description: string;
  public price: number;
  public category: string;
  public __v: number;

  constructor(_id, name, description, price, category, __v) {
    this._id = _id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.category = category;
    this.__v = __v;
  }
}
