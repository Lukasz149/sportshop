import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "./product";

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;
  @Output() addProductToCartEvent =  new EventEmitter<Product>();

  addProductToCart(product: Product) {
    this.addProductToCartEvent.emit(product);
  }

  constructor() { }

  ngOnInit() {
  }

}
