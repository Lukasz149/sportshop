import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Order} from "../../../../sport-shop/src/app/order";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  @Input() token;
  orders;
  detailOfOrder;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getOrders();
  }

  getOrders() : void {
    this.http.get("http://localhost:5000/orders", {
      headers: new HttpHeaders().set('Authorization', this.token),
    }).subscribe(data => {
        this.orders = data;
    })
  }

  getValueOfOrder(order : Order) : number {
    let val = 0;
    for(let i = 0; i < order.products.length; i++) {
      val += order.products[i]["product"].price * order.products[i]["amount"];
    }
    return val;
  }

  showDetails(order) {
    if(this.detailOfOrder == order._id) {
      this.detailOfOrder = 0;
    }
    else {
      this.detailOfOrder = order._id;

    }
  }
}
