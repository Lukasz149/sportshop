import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from "../user";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  @Output() changeViewEvent =  new EventEmitter<string>();
  @Output() loginEvent =  new EventEmitter<string>();

  model: User = new User('', '');
  constructor(private http: HttpClient) { }

  onSubmit() {
    this.http.post("http://localhost:5000/signin", this.model).subscribe(data => {
      if(data["success"]) {
        this.changeView("app");
        this.loginEvent.emit(data["token"]);
      }
    });
  }

  changeView(view: string) {
    this.changeViewEvent.emit(view);
  }

}
