import {Component, Input} from '@angular/core';
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  token: string = "";
  currentView = 'login';
  constructor(private http: HttpClient) { }


  changeViewEvent(view: string) {
    this.changeView(view);
  }

  changeView(view: string) : void {
    this.currentView = view;
  }

  loginEvent(token: string) : void {
    this.token = token;
  }

  getProducts() {
    this.http.get("http://localhost:5000/products").subscribe(data => {
      return data;
    });
  }
}
