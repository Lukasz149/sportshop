import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Product} from "../../../../sport-shop/src/app/product/product";
import {getProdConfig} from "@angular/cli/models/webpack-configs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @Input() token;
  products;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() : void {
    this.http.get("http://localhost:5000/products").subscribe(data => {
      this.products = data;
    });
  }

  editProduct(product: Product) {
    this.http.put("http://localhost:5000/product/" + product._id, product, {
      headers: new HttpHeaders().set('Authorization', this.token),
    }).subscribe(data => {

    })
  }

  deleteProduct(product: Product) {
    this.http.delete("http://localhost:5000/product/" + product._id, {
      headers: new HttpHeaders().set('Authorization', this.token),
    }).subscribe(data => {
      if(data["success"]) {
        this.getProducts();
      }
    })
  }
}
