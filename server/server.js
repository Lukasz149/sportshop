var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var bodyParser = require('body-parser');
require('./config/passport')(passport);
var jwt = require('jsonwebtoken');

var User = require('./User');

var app = express();

app.use(passport.initialize());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var Schema = mongoose.Schema;

var Products = new Schema({
    name: String,
    description: String,
    price: Number,
    category: String
});

var Orders = new Schema({
    name: String,
    surname: String,
    street: String,
    houseNumber: Number,
    apartmentNumber: Number,
    postCode: String,
    products: []
});

mongoose.model('Product', Products);
mongoose.model('Order', Orders);


mongoose.connect('mongodb://localhost/sport-shop', {
    useMongoClient: true
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'błąd połączenia...'));
db.once('open', function() {
    console.log("połączenie udane");
});

app.get('/orders', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        var Order = mongoose.model('Order');
        Order.find({}, function(err, orders) {
            if (err) throw err;
            console.log('Znaleziono takie zamowienia: ');
            console.log(orders);
            res.send(orders);
        });
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

app.put('/product/:id', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        var product = req.body;
        console.log(req.body);
        var Product = mongoose.model('Product');
        Product.update( {_id: req.params.id}, product, {multi:false}, function(err, rows_updated) {
                if (err) throw err;
                console.log("uaktualniono" + rows_updated);
                res.status(200).send({success: true});
            }
        );
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

app.delete('/product/:id', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        var Product = mongoose.model('Product');
        Product.findById(req.params.id, function(err, task) {
            task.remove();
            res.status(200).send({success: true});
        });
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

app.delete('/orders', function(req, res) {
    var Order = mongoose.model('Order');
    Order.find({}, function(err, task) {
        for(var i = 0; i < task.length; i++) {
            task[i].remove();

        }
        res.status(200).send({success: true});
    });
});

app.post('/admin', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
});

var getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

app.post('/signup', function(req, res) {
    if (!req.body.username || !req.body.password) {
        res.json({success: false, msg: 'Please pass username and password.'});
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                return res.json({success: false, msg: 'Username already exists.'});
            }
            res.json({success: true, msg: 'Successful created new user.'});
        });
    }
});

app.post('/signin', function(req, res) {
    User.findOne({
        username: req.body.username
    }, function(err, user) {
        if (err) throw err;
        if (!user) {
            res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    var token = jwt.sign(user.toObject(), 'ihiuhiuhiuhuihui');
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
                }
            });
        }
    });
});


app.post('/products', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    var data = req.body;
    for (var i = 0; i < data.length; i++) {
        var Product = mongoose.model('Product');
        var product = new Product();
        product.name = data[i].name;
        product.description = data[i].description;
        product.price = data[i].price;
        product.category = data[i].category;
        product.save(function(err) {
            if (err) throw err;
            console.log('Produkty został o zapisane.');
        });
    }
    res.sendStatus(200);
});

app.get('/products', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    var Product = mongoose.model('Product');
    Product.find({}, function(err, products) {
        if (err) throw err;
        console.log('Znaleziono takie produkty: ');
        console.log(products);
        res.send(products);
    });
});

app.get('/products/category/:category', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    var Product = mongoose.model('Product');
    Product.find({'category': req.params.category}, function(err, products) {
        if (err) throw err;
        console.log('Znaleziono takie produkty w kategorii ' + req.params.category + ': ');
        console.log(products);
        res.send(products);
    });
});

app.post('/orders', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    var data = req.body;
    var Order = mongoose.model('Order');
    var order = new Order();
    order.name = data.name;
    order.surname = data.surname;
    order.street = data.street;
    order.houseNumber = data.houseNumber;
    order.postCode = data.postCode;
    order.apartmentNumber = data.apartmentNumber;
    order.products = data.products;
    order.save(function(err) {
        if (err) throw err;
        console.log('Zamówienie zostało o zapisane.');
    });
    res.status(200).send({success: true});
});

var server = app.listen(5000, function () {
    var host = server.address().address;
    var port = server.address().port;
});

